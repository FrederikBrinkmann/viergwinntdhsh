package Server;

import java.io.*;
import java.net.*;

public class GameServer {

	private ServerSocket ss;
	private int numPlayers;
	private static int maxPlayers;
	private int port = 4242;
	
	
	private Socket p1Socket;
	private Socket p2Socket;
	private ReadFromClient p1ReadRunnable;
	private ReadFromClient p2ReadRunnable;
	private WriteToClient p1WriteRunnable;
	private WriteToClient p2WriteRunnable;
	
	private String clicked;
	private String p1engine;
	private String p2engine;
	
	
	public GameServer() {
		System.out.println("Spielserver");
		numPlayers = 0;
		maxPlayers = 2;
		
			try {
				ss = new ServerSocket(port);
			} catch (IOException ex) {

				System.out.println("IOException GameServer Costurcot");
			}
			System.out.println("Server gestartet!");
	
	
	}

	//Akzeptiert Anfragen 
	public void acceptConnection() {
		try {
		
		System.out.println("Warte auf Spieler...");
		
		while(numPlayers < maxPlayers)
		{
			Socket s;

				s = ss.accept();

				
				DataInputStream in = new DataInputStream(s.getInputStream());
				DataOutputStream out = new DataOutputStream(s.getOutputStream());
				

				

			numPlayers++;
			
			out.writeInt(numPlayers);


			System.out.println("Spieler " + numPlayers + " verbunden");
			
			ReadFromClient rfc = new ReadFromClient(numPlayers, in);
			WriteToClient wtc = new WriteToClient(numPlayers, out);
			
			if(numPlayers == 1)
			{
				p1Socket = s;
				p1ReadRunnable = rfc;
				p1WriteRunnable = wtc;
			}else
			{
				p2Socket = s;
				p2ReadRunnable = rfc;
				p2WriteRunnable = wtc;

				p1WriteRunnable.sendLoadMsg();
				p2WriteRunnable.sendLoadMsg();

				
			}
			
		}
		
		System.out.println("Alle verbunden!");
		Thread writeThread1 = new Thread(p1WriteRunnable);
		Thread writeThread2 = new Thread(p2WriteRunnable);
		writeThread1.start();
		writeThread2.start();
		
	} catch (IOException e) {

		e.printStackTrace();
	}
	

}
	

	private class ReadFromClient implements Runnable{

		
		private int playerNo;
		private DataInputStream dataIn;
		
		public ReadFromClient(int pid, DataInputStream in)
		{
			playerNo = pid;
			dataIn = in;
			System.out.println("Read from Client " + playerNo);
		}
	
		public void run() {
			try {
				
				while(true) {
					if (playerNo == 1)
							{		
						clicked  = dataIn.readUTF();
						if(clicked == "clicked") {
							System.out.println("Spieler 1 gelickt");
						}
								
							}
					else {
						clicked  = dataIn.readUTF();
						}
				}
				
			}catch(IOException ex) {
				System.out.println("IOException from RFC run()");
			}
			
			}
		

		}
		
	private  class WriteToClient implements Runnable{

		
		private int playerNo;
		private DataOutputStream dataOut;
		
		public WriteToClient(int pid ,DataOutputStream out)
		{
			playerNo = pid;
			dataOut = out;
			System.out.println("Write to Client " + playerNo);
		}
	
		public void run() {
	while(true) {

		
		sendLoadMsg();

		
		
		try {
			Thread.sleep(50);
		}catch(InterruptedException ex) {
			System.out.println("InterruptedException from WTC Run");
		}
	}

		
	}
					

    	public void sendLoadMsg() {
    		
    		try {
    			
    			dataOut.writeUTF("Laden");
    			
    			}catch(IOException ex) {
    				System.out.println("IOException sentStartMsg()");
    			}
    		
    		
    	}
    	
		
	}
		
	public static void main(String[] args) {
		GameServer gs = new GameServer();
		gs.acceptConnection();
		
	}

	
}
