package View;

import javax.swing.*;

import Model.SpielLogik;
import Model.GameSaver;
import Model.Move;
import Model.OutsideBoardException;
import Model.Spieler;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.*;
import java.util.concurrent.TimeUnit;


public class GUI extends MouseAdapter implements Serializable{
    /**
     * Icons der Spielsteine
     */
    private static final String ICON_LEER = "VierGewinnt/src/img/leer_01.png";
    private static final String ICON_ROT   = "VierGewinnt/src/img/rot_01.png";
    private static final String ICON_GELB = "VierGewinnt/src/img/gelb.png";
	private static final String PATH = "VierGewinnt/src/Savegame/saveBoard.json";
	private static final String PATHSCORE = "VierGewinnt/src/Savegame/saveScore.bin";
	private static final String PATHTURN = "VierGewinnt/src/Savegame/saveTurn.bin";

    private static final int        IMG_SIZE = 50;
    private              SpielLogik engine   = null;
    private JFrame frame;

   //F�r Server
    
    private String hostName = "localhost";
    private int port = 4242;
    private String load;
    private int playerNo = 0;
    private Socket socket;
    private ReadFromServer rfsRunnable;
   private WriteToServer wtsRunnable;
   private boolean netGame = false;

    /**
     * board wird als Array dargestellt
     * Jede Position wird durch ein Bild befllt
     */
    private  JLabel[][] board = null;
    private JLabel score;
    private JLabel currentTurn = null;
    private JMenuBar menuBar;
    
    
    /**
     * Mgliche Werte fr einen Spielstein, der in eine Spalte gesetzt wird
     * Default nimmt an, Spalte sei leer
     */
    private enum Disc
    {
        None,
        Spieler1,
        Spieler2
    }

    /**
     * Fenstererstellung (GUI)
     */
    public GUI(Spieler p1, Spieler p2)
    {
         frame = new JFrame();
        frame.setTitle("Vier Gewinnt!");
        score = new JLabel();
        menuBar = new JMenuBar();
        frame.setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        engine = SpielLogik.getInstance(p1, p2);
        createMenu();

    }
    
    /**
     * Spiel wird in festgelegter Gre gestartet
     * listener wird hinzugefgt um Input sicherzustellen

     */
    public void startGame()
    {
        initboard();
        frame.setSize(800, IMG_SIZE * 9);
        frame.addMouseListener(this);
    }

    /**
     * Obere Menleiste
     */
    public final void createMenu()
    {
        JMenu file = new JMenu("Spiel");
        
        JMenuItem newGame = new JMenuItem("Neue lokale Runde");
        
        
        newGame.addActionListener(new ActionListener() { 
        	  public void actionPerformed(ActionEvent e) { 
        	    //selectionButtonPressed();
//        	    GameSaver.saveGame(board, score, currentTurn);
//        		GameSaver.saveGame(frame);
        		initboard();
        		updateboard();
        	  } 
        	} );
        file.add(newGame);
        menuBar.add(file);
        
        JMenuItem networkGame = new JMenuItem("Netzwerkspiel");
        
        
        networkGame.addActionListener(new ActionListener() { 
        	  public void actionPerformed(ActionEvent e) { 

        	       connectServer();
        	       netGame = true;

        	  } 
        	} );
        file.add(networkGame);
        menuBar.add(file);
 
        
        JMenuItem saveGame = new JMenuItem("Spiel speichern");
        
        
        saveGame.addActionListener(new ActionListener() { 
        	  public void actionPerformed(ActionEvent e) { 

        		serializeGameModel();
        		
        	  } 
        	} );

        file.add(saveGame);
        menuBar.add(file);
        
        
        JMenuItem loadGame = new JMenuItem("Spiel laden");
        
        
        loadGame.addActionListener(new ActionListener() { 
        	  public void actionPerformed(ActionEvent e) { 

        		  deserializeGameModel();
        	  } 
        	} );

        file.add(loadGame);
        menuBar.add(file);
               
        
        JMenuItem exitItem = new JMenuItem("Verlassen");
        exitItem.addActionListener(new ExitApp(frame));
        file.add(exitItem);
        menuBar.add(file);
        JMenu help = new JMenu("Hilfe");
        JMenuItem helpItem = new JMenuItem("Anleitung");
        helpItem.addActionListener(new HelpMenu(frame));
        helpItem.setSize(300, 200);
        help.add(helpItem);
        menuBar.add(help);
        frame.setJMenuBar(menuBar);
    }

    
	public void serializeGameModel() {
		
		try {
			FileOutputStream outputFile = new FileOutputStream(PATH);
			ObjectOutputStream oos = new ObjectOutputStream(outputFile);
			oos.writeObject(engine);
			oos.close();
			outputFile.close();
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}
    }
	
	
	public boolean deserializeGameModel() {

		
		try {
			FileInputStream inputFile = new FileInputStream(PATH);
			ObjectInputStream ois = new ObjectInputStream(inputFile);
			this.engine =  (SpielLogik) ois.readObject();
			
			updateboard();

			System.out.println("Found a serialized file.");
			ois.close();
			inputFile.close();
			return true;
		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
			return false;
		} catch (ClassNotFoundException ex) {
			System.err.println(ex.getStackTrace());
			return false;
		}
	}
    
    /**
     * boardaktualisierung
     * Austausch des alten Bildes (leer = wei) mit dem jeweilig neuen des Spielers (schwarz / rot) um Spielstein zu "setzen"
     */
    public void updateboard()
    {
        int[][] a = engine.getBoard().getBoardArray();
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                Disc pos = Disc.values()[a[i][j]];
                switch (pos) {
                    case None:
                        board[i][j].setIcon(new ImageIcon(ICON_LEER ));
                        break;
                    case Spieler1:
                        board[i][j].setIcon(new ImageIcon(ICON_ROT));
                        break;
                    case Spieler2:
                        board[i][j].setIcon(new ImageIcon(ICON_GELB));
                        break;
                }
            }
        }  
        if(netGame == true)
        {
        serializeGameModel(); 
        }
    }

    /**
     * Wird aufgerufen sobald das Spiel vorrber ist.
     * Gibt den Spielern zwei Entscheidungen
     * 1. Erneut spielen
     * 2. Spiel verlassen
     * Gibt ebenfalls den Gewinner bekannt, sofern es einen gibt
     */
    public boolean gameOver(Spieler winner)
    {
        String message = "Game Over, Draw.";
        if (winner.getInt() != 0) {
            message = String.format("Spiel vorbei, Spieler %d gewinnt.", winner.getInt());
        }
        int playAgain = JOptionPane.showConfirmDialog(frame, message, "Spiel vorbei, neue Runde?", JOptionPane.YES_NO_OPTION);

        if (playAgain == JOptionPane.YES_OPTION) {
            initboard();
            updateboard();

            return false;
        }
        return true;
    }

    public void updateScoreText()
    {
        int[] SpielerScores = engine.getScore();
        score.setText(String.format("Spielstand: %s - %s", SpielerScores[0], SpielerScores[1]));
  
    }

    public void updateTurnText(Spieler currentSpieler)
    {
        currentSpieler = currentSpieler == null ? new Spieler(1) : currentSpieler;
        String color = currentSpieler.getInt() == 1 ? "Rot" : "Gelb";
        currentTurn.setText(String.format("Spieler %s (%s) an der Reihe!", currentSpieler.getInt(), color));
    }

    /**
     * Fgt den Spielstein des jeweiligen Spielers dem board hinzu

     * columnNumber = Spalte in der der Spielstein hinzugefgt werden soll
     * Rckgabewert true wenn der Spielstein erfolgreich hinzugefgt werden konnte (Spalte nicht voll)
     */
    public boolean putDisc(int columnNumber) throws OutsideBoardException
    {
        boolean putIsDone = engine.putDisc(new Move(columnNumber));
        updateTurnText(engine.getAktuellenSpieler());
        if (putIsDone) {
            Spieler p = engine.isGameOver();
            updateboard();
            updateScoreText();
            if (p != null) {
                boolean noMoreGames = gameOver(p);

                if (noMoreGames) {
                    frame.removeMouseListener(this);
                }
            }
        }
        return putIsDone;
        
    }

    /**
     * Stellt fest an welcher Stelle der Mausklick erfolgte
     * Weist Spielstein der geklickten Spalte zu
     */
    @Override
    public void mousePressed(MouseEvent mouseEvent)
    {
        try {
            if (mouseEvent.getY() < IMG_SIZE * (engine.getRowNumber() + 0.5f) &&
                mouseEvent.getX() < IMG_SIZE * engine.getColumnNumber()) {
                putDisc(mouseEvent.getX() / IMG_SIZE);
//                wtsRunnable.moveMade();
            }
        } catch (OutsideBoardException ignored) {
        }

    }
    



    //Initialisiert ein neues board und weit jeder Position einen leeren (wei) Platz zu


    private void initboard()
    {
        engine.clearBoard();
        frame.getContentPane().removeAll();
        board = new JLabel[engine.getRowNumber()][engine.getColumnNumber()];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                board[i][j] = new JLabel();
                board[i][j].setBounds(j * IMG_SIZE, i * IMG_SIZE, IMG_SIZE, IMG_SIZE);
                board[i][j].setIcon(new ImageIcon(ICON_LEER));
                frame.getContentPane().add(board[i][j]);
            }
        }
        //bersicht ber Spielstand und welcher Spieler an der Reihe ist zu setzten
        currentTurn = new JLabel();
        currentTurn.setBounds(8 * IMG_SIZE, 0, 200, 20);
        score.setBounds(12 * IMG_SIZE, 0, 200, 20);
        updateScoreText();
        updateTurnText(null);
        frame.getContentPane().add(currentTurn);
        frame.getContentPane().add(score);
        frame.setVisible(true);
    }

    
    private void connectServer() {
    	System.out.println(playerNo);
    	
    	try {
    		
    		socket = new Socket("localhost", port);
//    		
//			ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
//			ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
			
			DataOutputStream out = new DataOutputStream(socket.getOutputStream());
			DataInputStream in = new DataInputStream(socket.getInputStream());
			
//			BufferedReader socketIn = new BufferedReader(new InputStreamReader(socket.getInputStream())); // Inputstream vom Client
//			PrintWriter socketOut = new PrintWriter(socket.getOutputStream(), true); // Outputstream zum Client mit autoflush
    		
			
			
			playerNo = in.readInt();
			System.out.println( playerNo);

			
			if(playerNo == 1) {
				System.out.println("Du bist Rot!");
				System.out.println("Warte auf zweiten Spieler....");
			}
			
			rfsRunnable = new ReadFromServer(in);
			wtsRunnable = new WriteToServer(out);
			
//			rfsRunnable.waitForStart();
			rfsRunnable.waitToLoad();
			
//			rfsRunnable.run();
			
			
    		}catch(IOException ex) {
    			System.out.println("IOException vom Client");
    		}
    	
    }
    
 

    
    
    private class ReadFromServer implements Runnable{

    	private DataInputStream dataIn;
    	
    	public ReadFromServer(DataInputStream in) {
    		

    		dataIn = in;
    		System.out.println("RFS Runnable erstellt");

    	}
    	
    	public void run() {
    		
    	}
    	
    	public void waitToLoad() {
    		
    		try {

    			String loadMsg = dataIn.readUTF();
    			System.out.println("Nachricht vom Server: " + loadMsg);
    			
    			deserializeGameModel();
    			updateboard();
    			
//    			Thread waitLoad = new Thread(rfsRunnable);
//    			waitLoad.start();

    			
    			}catch(IOException ex) {
    				System.out.println("IOException waitToLoad()");
    			}
    		
    		
    	}

    }
    
    private class WriteToServer implements Runnable{

    	private DataOutputStream dataOut;
    	
    	public WriteToServer(DataOutputStream out) {
    		
    		dataOut = out;
    		System.out.println("WTS Runnable erstellt");
    		
    		
    	}
    	
    	public void run() {
    		

    	}

        	public void moveMade() {
        		
        		try {
        			dataOut.writeUTF("clicked");
        			System.out.println("Spieler " + playerNo +  " Zug uebermittelt" );
        			
        			
       			
        			}catch(IOException ex) {
        				System.out.println("IOException waitToLoad()");
        			}
        		
        		
        	

        }
    		
    		

    }
    
    
    
    
    
    
    
    
    
//    private void gameType()
//    {
//    	
////    int gameType = JOptionPane.showConfirmDialog(frame, "Ist dies eine lokale Runde?", null, JOptionPane.YES_NO_OPTION);
////   	 
////   	 if(gameType == JOptionPane.YES_OPTION)
////   	 	{
////            initboard();
////            updateboard();
////   	 	}
////   	 else
////   	 	{
////   		 
//    		
//    		
//    		
//   		 	try(Socket socket = new Socket(hostName, port))
//   		 	{
//
//  		      DataInputStream fromPlayer1 = new DataInputStream(
//		    	        socket.getInputStream());
//		    	      DataOutputStream toPlayer1 = new DataOutputStream(
//		    	        socket.getOutputStream());
//		    	      
//		    	      
//   		 		
//   			ObjectOutputStream outStream = new ObjectOutputStream(socket.getOutputStream());
//   			ObjectInputStream inStream = new ObjectInputStream(socket.getInputStream());
//
//   			playerNo = fromPlayer1.readInt();
//   			
//   			System.out.println(playerNo);
//
//   			
//   		 	}
//   		 	catch (UnknownHostException ue) {
// 			System.out.println("Kein DNS-Eintrag fuer " + hostName);
//   		 	} catch (NoRouteToHostException e) {
// 			System.err.println("Nicht erreichbar " + hostName);
//   		 	} catch (IOException e) {
// 			System.out.println("IO-Error GUI");
//   		 	}
//  		 	
//   		 
////   	 	}
//    	
//    }
//   
//    public void austausch() {
//    	try(Socket socket = new Socket(hostName, port))
//    	{
//   			ObjectOutputStream outStream = new ObjectOutputStream(socket.getOutputStream());
//   			ObjectInputStream inStream = new ObjectInputStream(socket.getInputStream());
//   			
//   			if(playerNo == 1)
//   			{
//   				outStream.writeObject(engine);
//   			}
//   			else
//   			{
//   				this.engine = (SpielLogik) inStream.readObject();
//   				updateboard();
//   			}
//    		
//    	} catch (UnknownHostException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (ClassNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//    	
//    	
//    }
    

    
    

    //Fgt Kontextmen fr das Beenden der Anwendung hinzu


    private static class ExitApp implements ActionListener
    {
        private JFrame frame;

        ExitApp(JFrame frame)
        {
            this.frame = frame;
        }

        @Override
        public void actionPerformed(ActionEvent e)
        {
            int playAgain = JOptionPane.showConfirmDialog(frame, "Mchten Sie das Spiel wirklich beenden??", "", JOptionPane.YES_NO_OPTION);

            if (playAgain == JOptionPane.YES_OPTION) {
                System.exit(0);
            }
        }
    }


    /**
     * Kontextmen fr die Anleitung
     */
    private static class HelpMenu implements ActionListener
    {
        private JFrame frame;

        HelpMenu(JFrame frame)
        {
            this.frame = frame;
        }

        @Override
        public void actionPerformed(ActionEvent e)
        {
            String msg = "Author Finn Ole Schmidt \n\nSetzt abwechselnd Spielsteine in die Reihen. \n" +
                "Ziel ist es, vier gleichfarbige Spielsteine in eine Reihe zu bringen (horizontal, vertikal oder diagonal). \n" +
                "Der erste, der vier Spielsteine in einer Reihe verbunden hat, gewinnt.\n";

            JOptionPane.showMessageDialog(frame, msg, "Spielanleitung", JOptionPane.OK_OPTION);
        }
    }
}



