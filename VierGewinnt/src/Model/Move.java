package Model;

import java.io.Serializable;

public class Move implements Serializable
{
    private int position;

    /**

     * position -  Spalte des Spielzugs
     */
    public Move(int position)
    {
        this.position = position;
    }

    /**
     * Wo finded der Spielzug statt?
     * R�ckgabewert entspricht der Position des Spielzuges
     */
    public int getPosition()
    {
        return position;
    }
}
