package Model;

import java.io.Serializable;

public class Spieler implements Serializable
{
    private int spielerNum;
    public int wins = 0;

    /**
     * Spielerobjekt 
     * A player object to keep track of game players
     * playerNum = Spieler-ID
     */
    public Spieler(int spielerNum)
    {
        this.spielerNum = spielerNum;
    }

    /**
     * Getter der Spieler-ID
     *
     * R�ckgabewert entspricht Spieler-ID
     */
    public int getInt()
    {
        return spielerNum;
    }
    


    
    
}