package Model;

public class OutsideBoardException extends Exception
{
    private static final long serialVersionUID = 799526730449750418L;

    /**
     * Individuelle Exception f�r den Fall, dass ein un�ltiger Spielzug vorgenommen wird.
     * UNg�ltig bedeutet, dass diese nicht auf dem Spielfeld stattfinden oder wenn alle Spalten und
     * Zeilen voll sind.
     */
    @Override
    public String getMessage()
    {
        return "Der aktuelle Spielzug befindet sich au�erhalb des Spielfeldsd.";
    }
}
