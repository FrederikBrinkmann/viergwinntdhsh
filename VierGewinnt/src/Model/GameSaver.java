package Model;



import View.GUI;import View.GameBoard;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.JFrame;
import javax.swing.JLabel;


// Speichern und laden von Spielständen

public class GameSaver {
	
	 private static final String PATH = "src/Savegame/savegame.bin";
	

	public static void saveGame()
	 {
		 try (ObjectOutputStream fos = new ObjectOutputStream(new FileOutputStream(PATH))){
			 
			 fos.writeObject(GameBoard.getInstance());
			 
		 } catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		 
	 }
	
}
