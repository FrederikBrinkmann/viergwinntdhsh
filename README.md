# VierGwinntDHSH

VierGewinnt:

Die Main-Methode befindet sich innerhalb der Klasse main.java, mit dem Pfad VierGewinnt\src\Main

Start eines einfachen Spiels:

1. Run Main.java

Start eines Netzwerkaustausches:

1. Run GameServer.java mit dem Pfad VierGewinnt\src\Server
2. Run Main.java (Client 1)
3. Run Main.java (Client 2)
4. Menü -> Netzwerkspiel
